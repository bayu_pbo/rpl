# 1.Tampilan Peserta Lomba

1. Tampilan Home
![Dokumentasi](Dokumentasi/1.png)


![Dokumentasi](Dokumentasi/2.png)

![Dokumentasi](Dokumentasi/3.png)

2. Tampilan Register
![Dokumentasi](Dokumentasi/5.png)

3. Tampilan Login
![Dokumentasi](Dokumentasi/4.png)

4. Tampilan Menu Peserta Lomba
![Dokumentasi](Dokumentasi/6.png)

5. Tampilan isi Biodata Peserta Lomba
![Dokumentasi](Dokumentasi/7.png)

6. Tampilan Menu Cari Dan Daftar Lomba
![Dokumentasi](Dokumentasi/8.png)
![Dokumentasi](Dokumentasi/9.png)

7. Tampilan Menu Pesanan Dan Cetak Tiket Peserta Dan Tiket Lomba
![Dokumentasi](Dokumentasi/10.png)
![Dokumentasi](Dokumentasi/11.png)

# 2.Tampilan Peserta Tour

![Dokumentasi](Dokumentasi/12.png)

![Dokumentasi](Dokumentasi/13.png)

![Dokumentasi](Dokumentasi/14.png)

![Dokumentasi](Dokumentasi/15.png)

![Dokumentasi](Dokumentasi/16.png)

# 3.Tampilan Admin

1. Tampilan Login Admin
![Dokumentasi](Dokumentasi/17.png)

2. Tampilan Home Admin
![Dokumentasi](Dokumentasi/18.png)

3. Tampilan Tambah Tipe Kendaraan
![Dokumentasi](Dokumentasi/19.png)

4. Tampilan Edit Tipe Kendaraan
![Dokumentasi](Dokumentasi/20.png)

5. Tampilan Tambah  Kendaraan
![Dokumentasi](Dokumentasi/21.png)

6. Tampilan Edit  Kendaraan
![Dokumentasi](Dokumentasi/22.png)

7. Tampilan Tambah Rute Tour
![Dokumentasi](Dokumentasi/23.png)

8. Tampilan Edit Tour
![Dokumentasi](Dokumentasi/24.png)


9. Tampilan Menu Reservasi dari Peserta
![Dokumentasi](Dokumentasi/25.png)

10. Tampilan Menu Tambah Lomba
![Dokumentasi](Dokumentasi/26.png)

11. Tampilan Edit Tambah Lomba
![Dokumentasi](Dokumentasi/27.png)

12. Tampilan Menu Tambah Tipe Lomba
![Dokumentasi](Dokumentasi/28.png)

13. Tampilan Edit Tipe Tambah Lomba
![Dokumentasi](Dokumentasi/29.png)

14. Tampilan Menu Peserta (User)
![Dokumentasi](Dokumentasi/30.png)
